import React from 'react';
import {ApolloProvider} from 'react-apollo';
import ApolloClient from 'apollo-boost';
import Root from "./components/root";

const client = new ApolloClient({uri: process.env.REACT_APP_API_URL});

const Application: React.FC = () => (
  <ApolloProvider {...{client}}>
    <Root/>
  </ApolloProvider>
);

export default Application;