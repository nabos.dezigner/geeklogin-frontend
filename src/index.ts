import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';

import React from 'react';
import ReactDOM from 'react-dom';
import Application from './Application';

const app = document.createElement("div");
app.dataset.application = 'geeklogin';
document.body.appendChild(app);
ReactDOM.render(React.createElement(Application), app);