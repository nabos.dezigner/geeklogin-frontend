import React from 'react';
import NewsContainer from "../news";

const MainPageContainer = () => {
  return (
    <div>
      <NewsContainer/>
    </div>
  );
};

export default MainPageContainer;