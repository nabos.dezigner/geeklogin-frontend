export type StreamType = {
  id: string;
  title: string;
  viewer_count: number;
  user_name: string;
  thumbnail_url: string;
}