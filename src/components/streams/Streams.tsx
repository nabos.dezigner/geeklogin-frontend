import React, {useState} from 'react';
import {StreamType} from "./streams.types";

type StreamsType = {
  streams?: StreamType[];
};

const Streams: React.FC<StreamsType> = (
  {
    streams = []
  }
) => {
  const [view, setView] = useState('');
  
  const img = (url: string) => (
  	url
			.replace('{width}', '400')
			.replace('{height}', '250')
  );
  
  return (
    <div>
      {streams.map(stream => (
        <div key={stream.id}>
          <img src={img(stream.thumbnail_url)} alt=""/>
          <ul>
            <li>Название стрима: {stream.title}</li>
            <li>Сейчас смотрит: {stream.viewer_count}</li>
            <li>Кто вещает: {stream.user_name}</li>
          </ul>
          {view === stream.user_name
            ? <button onClick={() => setView('')}>Закрыть этот стрим</button>
            : <button onClick={() => setView(stream.user_name)}>Смотреть этот стрим</button>
          }
          <br/><br/>
          {view === stream.user_name && (
            <iframe
              src={`https://player.twitch.tv/?channel=${stream.user_name}`}
              height="500px"
              width="1000px"
              frameBorder="0"
              scrolling="none"
              allowFullScreen={true}
              title={stream.id}
            />
          )}
          <br/><br/>
        </div>
      ))}
    </div>
  );
};

export default Streams;