import React from 'react';
import gql from "graphql-tag";
import Streams from "./Streams";
import {Empty} from "antd";
import {useQuery} from "@apollo/react-hooks";
import {StreamType} from "./streams.types";

const GET_STREAMS = gql`
	query {
		streams {
			id
			title
			viewer_count
			user_name
			thumbnail_url
		}
	}
`;

type DataType = {
  streams: StreamType[];
}

const StreamsContainer = () => {
  const {loading, error, data} = useQuery<DataType>(GET_STREAMS);
  
  if (loading) return <p>Loading...</p>;
  if (error) return <Empty description={false}/>;
  return <Streams {...{...data}}/>;
};

export default StreamsContainer;