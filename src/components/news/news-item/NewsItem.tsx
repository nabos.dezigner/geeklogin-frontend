import React from "react";
import {ItemType} from "./news-item.types";

const NewsItem: React.FC<ItemType> = (
  {
    headline = 'Ошибка загрузки новости',
    categories: {name: catName = ''},
    description = '',
    body = '',
    tags = [],
    creator = '',
    creation_date = '',
  }
) => {
  return (
    <div>
      <h1>{headline}</h1>
      <h2>{description}</h2>
      <main>{body}</main>
      <menu>
        <li>
          <span>Категория</span>
          <span>{catName}</span>
        </li>
        <li>
          <span>Теги</span>
          {tags.map(({tag}, index) => <span key={index}>{tag}</span>)}
        </li>
        <li>
          <span>Создал</span>
          <span>{creator}</span>
        </li>
        <li>
          <span>Дата создания</span>
          <span>{creation_date}</span>
        </li>
      </menu>
    </div>
  )
};

export default NewsItem;