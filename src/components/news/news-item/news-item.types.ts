type tagsType = {
  tag: string;
}

export type ItemType = {
  headline: string;
  categories: {
    name: string
  };
  description: string;
  body: string;
  tags: tagsType[];
  creator: string;
  creation_date: string;
}