import React from "react";
import gql from "graphql-tag";
import {useQuery} from "@apollo/react-hooks";
import {useParams} from 'react-router-dom';
import {Empty} from "antd";
import {ItemType} from "./news-item.types";
import NewsItem from "./NewsItem";

const GET_NEWS_ITEM = gql`
	query NewsItem($id: ID!) {
    news_item(id: $id){
      headline
      categories {
        name
      }
      description
      body
      tags {
        tag
      }
      creator
      creation_date
    }
	}
`;

type DataType = {
  news_item: ItemType;
}

const NewsItemContainer: React.FC = () => {
  const {id} = useParams();
  const {loading, error, data} = useQuery<DataType>(GET_NEWS_ITEM, {variables: {id}});
  
  if (loading) return <p>Loading...</p>;
  if (error) return <Empty description={false}/>;
  return <NewsItem {...{...data!.news_item}}/>
};

export default NewsItemContainer;