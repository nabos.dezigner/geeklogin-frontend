import React from 'react';
import {NewsContainer, NewsItem} from "./styled";
import {NewsType} from "./news.types";

type NewsProps = {
  news?: Array<NewsType>,
  goToNews: (id: string) => void
}

const News: React.FC<NewsProps> = (
  {
    news = [],
    goToNews
  }
) => {
  return (
    <NewsContainer>
      {news.map((
        {
          id,
          headline,
          categories,
          description
        }
      ) => (
        <NewsItem key={id} onClick={() => goToNews(id)}>
          <h2>{headline}</h2>
          <p>{description}</p>
          <span>{categories.name}</span>
        </NewsItem>
      ))}
    </NewsContainer>
  )
};

export default News;