import React, {useEffect} from 'react';
import gql from "graphql-tag";
import {Empty} from "antd";
import News from "./News";
import {useQuery} from "@apollo/react-hooks";
import {useHistory} from 'react-router-dom'
import {NewsType} from "./news.types";

const GET_NEWS = gql`
	query {
    news {
      id
      headline
      categories { name }
      description
    }
	}
`;

type DataType = {
  news: NewsType[];
}

const NewsContainer: React.FC = () => {
  const {loading, error, data, refetch} = useQuery<DataType>(GET_NEWS);
  const {push} = useHistory();
  
  useEffect(() => {
    const refetchNews = setInterval(() => refetch(GET_NEWS), 500000);
    
    return () => {
      clearInterval(refetchNews);
    }
  }, [refetch]);
  
  const goToNews = (id: string) => {
    push(`/news/${id}`)
  };
  
  if (loading) return <p>Loading...</p>;
  if (error) return <Empty description={false}/>;
  return <News {...{...data, goToNews}}/>;
};


export default NewsContainer;