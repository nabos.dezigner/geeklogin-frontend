import styled from "@emotion/styled";

export const NewsContainer = styled.div`
  display: flex;
`;

export const NewsItem = styled.div`
  background-color: white;
  border-radius: 20px;
  padding: 15px;
  margin: 10px 5px;  
`;