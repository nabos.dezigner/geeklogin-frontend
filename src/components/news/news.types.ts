export type NewsType = {
  id: string;
  headline: string;
  categories: { name: string };
  description: string;
}