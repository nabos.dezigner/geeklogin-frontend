import React from 'react';
import Navigate from "./Navigate";
import {rootRoutes} from "../../root/routes";

const HeaderContainer = () => {
  return <Navigate routes={rootRoutes}/>
};

export default HeaderContainer;