import React, {Fragment, useState} from 'react';
import PropTypes from 'prop-types';
import Styled from '../../../UI/utils/Header';
import logo from './logo.svg';
import {NavLink} from "react-router-dom";
import styled from "@emotion/styled";
import {defaultSize} from "../../../UI";

const Logo = styled.img`
  height: 64px;
  transition: .3s;
  
  @media screen and (max-width: ${defaultSize.tablet}px) {
    height: 54px;
  }
  
  @media screen and (max-width: ${defaultSize.mobile}px) {
    height: 44px;
  }

`;

const Navigate = ({routes}) => {
  return (
    <Styled.Header>
      <Logo src={logo} alt="logo" />
      <Nav routes={routes} />
    </Styled.Header>
  );
};

const Nav = ({routes}) => {
  const [activeLink, setActiveLink] = useState();
  const [isNavActive, setNavActive] = useState();
  const setNavActiveStatus = () => setNavActive(!isNavActive);
  return (
    <Fragment>
      <Styled.Switcher
        isNavActive={isNavActive}
        onClick={setNavActiveStatus}
      >
        <span/><span/><span/>
      </Styled.Switcher>
      <Styled.Navigate isNavMobile={isNavActive}>
        <Styled.NavList>
          {routes.map(route => route.isNav && <Styled.NavItem
            key={route.path}
            isCurrentPath={route.path === activeLink}
          >
            <NavLink
              isActive={(match, location) => {
                if (!match) return false;
                setActiveLink(location.pathname);
                return true;
              }}
              to={route.path}
              onClick={setNavActiveStatus}
            >
              {route.name}
            </NavLink>
          </Styled.NavItem>)}
        </Styled.NavList>
      </Styled.Navigate>
    </Fragment>

  );
};

Navigate.propTypes = {
  routes: PropTypes.array,
};


Nav.propTypes = {
  routes: PropTypes.array,
};

export default Navigate;