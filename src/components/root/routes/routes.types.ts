import React from "react";

export type RoutesType = {
  isNav?: boolean
  isExact?: boolean
  isPrivate?: boolean
  path: string
  name: string
  component: React.FC
};