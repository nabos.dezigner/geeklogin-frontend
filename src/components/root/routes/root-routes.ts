import MainPage from "../../main-page";
import Streams from "../../streams";
import {RoutesType} from "./routes.types";
import NewsItemContainer from "../../news/news-item";

export const rootRoutes: Array<RoutesType> = [
  {
    isNav: true,
    isExact: true,
    isPrivate: false,
    path: '/',
    name: "Главная",
    component: MainPage
  },
  {
    isNav: true,
    isExact: true,
    isPrivate: false,
    path: '/streams',
    name: "Онлайн трансляции",
    component: Streams
  },
  {
    isNav: false,
    isExact: true,
    isPrivate: false,
    path: '/news/:id',
    name: "Новость",
    component: NewsItemContainer
  }
];