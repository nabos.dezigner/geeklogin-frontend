import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Normalize from "../../UI/utils/Normalize";
import Fonts from "../../UI/utils/Fonts";
import HeaderContainer from "../general/navigation";
import {rootRoutes} from "./routes";
import GlobalStyled from "../../UI/utils/Global";
import {RoutesType} from "./routes/routes.types";

const RootContainer: React.FC = () => (
  <Router>
    <Normalize/>
    <Fonts/>
    <GlobalStyled/>
    <HeaderContainer/>
    <div>
      <Switch>
        {rootRoutes.map((route: RoutesType) => (
          <Route
            key={route.path}
            exact={route.isExact}
            path={route.path}
            component={route.component}
          />
        ))}
      </Switch>
    </div>
  </Router>
);

export default RootContainer;