import styled from '@emotion/styled';
import {defaultSize, colors} from "../";

export const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: ${defaultSize.header}px;
  padding: 0 20px;
`;

type SwitcherPropsType = {
  isNavActive: boolean;
}

export const Switcher = styled.div<SwitcherPropsType>`
  position: relative;
  width: 21px;
  height: 21px;
  
  span {
    position: relative;
    display: block;
    width: 18px;
    height: 2px;
    margin-top: 3px;
    border: 1px solid ${colors.primary};
    margin-top: 3px;
    transition: .3s;
    border-radius: 10px;
    background-color: ${colors.primary};
  }
  
  ${props => props.isNavActive && `
    span {
      background-color: white;
      border-color: white;
    };
    span:first-child {transform: translateY(5px) translateZ(0) rotate(45deg);}
    span:nth-of-type(2) {opacity: 0;}
    span:nth-of-type(3) {transform: translateY(-9px) translateZ(0) rotate(-45deg);}
  `}
  
  @media screen and (min-width: ${defaultSize.tablet + 3}px) {
    display: none;
  }
`;

type NavigatePropsType = {
  isNavMobile: boolean;
}

export const Navigate = styled.nav<NavigatePropsType>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  
  @media screen and (max-width: ${defaultSize.tablet}px) {
    position: fixed;
    width: 100%;
    height: calc(100vh - 200px);
    top: 0;
    right: ${props => props.isNavMobile ? '0' : '-100%' };
    padding: 100px 0;
    text-align: center;
    background-color: ${colors.primary};
    transition: .75s;
  }
`;

export const NavList = styled.ul`
  display: flex;
  list-style: none;
  padding: 0;
  
  @media screen and (max-width: ${defaultSize.tablet}px) {
    flex-direction: column;
    width: 100%;
    height: 100%;
    justify-content: center;
    align-items: center;
    font-size: 2em;
  }
`;

type NavItemPropsType = {
  isCurrentPath: boolean;
}

export const NavItem = styled.li<NavItemPropsType>`
  a {
    display: block;
    text-decoration: none;
    color: ${props => props.isCurrentPath ? colors.primary : colors.GrayScale_900};
    font-weight: 600;
    font-size: 1.2em;
    padding: 0 15px;
    transition: .4s;
    
    &:hover {
      color: red;
    }
    
    @media screen and (max-width: ${defaultSize.tablet}px) {
      color: ${props => props.isCurrentPath ? colors.GrayScale_900 :  'white'};
      font-size: .9em;
    }
  }
`;


export default {Header, Navigate, NavList, NavItem, Switcher};