import React from "react";
import { Global, css } from '@emotion/core'

const Fonts = () => {
  return (
    <Global
      styles={css`
        @font-face { 
          font-family: "MagistralTT";
          src: url(${`http://${process.env.REACT_APP_URL}/MagistralTT.woff`}) format("woff")
        }
        body {font-family: "MagistralTT", Arial, sans-serif; }
      `}
    />
  )
};

export default Fonts;