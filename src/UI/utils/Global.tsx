import React from "react";
import {Global, css} from '@emotion/core'
import colors from "./colors";

const GlobalStyled = () => {
  return (
    <Global
      styles={css`        
        body {
          background-color: ${colors.backgroundPrimary};
        }
      `}
    />
  )
};

export default GlobalStyled;