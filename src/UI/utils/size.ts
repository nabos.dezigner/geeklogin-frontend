export const defaultSize = {
  header: 100,
  mobile: 768,
  tablet: 1024,
  desktop: 1280
};